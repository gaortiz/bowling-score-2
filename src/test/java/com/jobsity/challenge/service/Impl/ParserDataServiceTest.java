package com.jobsity.challenge.service.Impl;

import com.jobsity.challenge.service.impl.ParserDataService;
import org.junit.Assert;
import org.junit.Test;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ParserDataServiceTest {

    private ParserDataService parserDataService = new ParserDataService();

    @Test
    public void getGroupedShots_inputStreamOfLines_returnGroupedScore() {
        Stream<String> lines = Stream.of("Jeff 10",
                "John 3",
                "John 7",
                "Jeff 7",
                "Jeff 3",
                "John 6",
                "John 3",
                "Jeff 9",
                "Jeff 0",
                "John 10",
                "Jeff 10",
                "John 8",
                "John 1",
                "Jeff 0",
                "Jeff 8",
                "John 10",
                "Jeff 8",
                "Jeff 2",
                "John 10",
                "Jeff F",
                "Jeff 6",
                "John 9",
                "John 0",
                "Jeff 10",
                "John 7",
                "John 3",
                "Jeff 10",
                "John 4",
                "John 4",
                "Jeff 10",
                "Jeff 8",
                "Jeff 1",
                "John 10",
                "John 9",
                "John 0");

        final Map<String, List<String>> expectedResult =
                Stream.of(
                        new AbstractMap.SimpleImmutableEntry<>("John",
                                Arrays.asList("3", "7", "6", "3", "10", "8", "1", "10", "10", "9", "0", "7",
                                        "3", "4", "4", "10", "9", "0")),
                        new AbstractMap.SimpleImmutableEntry<>("Jeff",
                                Arrays.asList("10", "7", "3", "9", "0", "10", "0", "8", "8", "2", "F", "6",
                                        "10", "10", "10", "8", "1")))
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        final Map<String, List<String>> groupedShots = parserDataService.getGroupedShots(lines);
        Assert.assertEquals(expectedResult, groupedShots);
    }

}
