package com.jobsity.challenge.utils;

public class Errors {

    public static final String SHOT_VALUE_ERROR = "Shot values should be numbers from 0 to 10 or F letter";
    public static final String LINE_STRUCTURE_ERROR = "Line structure should be [Name] [Score] E.g. David 7";
    public static final String FRAME_NUMBER_ERROR = "Frames should be 10 per player";

}
