package com.jobsity.challenge.model;

import java.util.List;
import java.util.Map;

public class FramesResult {

    private List<Integer> score;
    private Map<Integer, List<String>> pinFalls;
    private String player;

    public FramesResult(List<Integer> score, Map<Integer, List<String>> pinFalls, String player) {
        this.score = score;
        this.pinFalls = pinFalls;
        this.player = player;
    }

    public List<Integer> getScore() {
        return score;
    }

    public Map<Integer, List<String>> getPinFalls() {
        return pinFalls;
    }

    public String getPlayer() {
        return player;
    }
}
