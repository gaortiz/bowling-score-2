package com.jobsity.challenge.model;

public class InputScore {

    private String name;
    private String score;

    public InputScore(String name, String score) {
        this.name = name;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public String getScore() {
        return score;
    }
}
