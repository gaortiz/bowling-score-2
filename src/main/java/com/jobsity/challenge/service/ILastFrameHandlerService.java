package com.jobsity.challenge.service;

import java.util.List;
import java.util.Map;

public interface ILastFrameHandlerService {

    void setLastFramePinFalls(Map<Integer, List<String>> pinFalls,
                              String actualShot, String nextShot, String afterNextShot,
                              int actualShotValue, int nextShotValue, int afterNextShotValue);

}
