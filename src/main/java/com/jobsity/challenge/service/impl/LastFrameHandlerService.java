package com.jobsity.challenge.service.impl;

import com.jobsity.challenge.service.ILastFrameHandlerService;
import com.jobsity.challenge.utils.Constants;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class LastFrameHandlerService implements ILastFrameHandlerService {

    public void setLastFramePinFalls(Map<Integer, List<String>> pinFalls,
                                     String actualShot, String nextShot, String afterNextShot,
                                     int actualShotValue, int nextShotValue, int afterNextShotValue) {
        if (actualShotValue + nextShotValue < Constants.MAX_PINFALL) {
            pinFalls.put(Constants.TOTAL_FRAMES, Arrays.asList(actualShot, nextShot));
        } else {
            String firstPinFall = actualShot.equals(Constants.MAX_PINFALL_STRING) ? Constants.X : actualShot;

            String secondPinFall = getLastFrameSecondPinFall(nextShot, actualShotValue, nextShotValue, firstPinFall);

            String lastPinFall = getLastFrameLastPinFall(afterNextShot, nextShotValue, afterNextShotValue, firstPinFall);

            pinFalls.put(Constants.TOTAL_FRAMES, Arrays.asList(firstPinFall, secondPinFall, lastPinFall));
        }
    }

    private String getLastFrameSecondPinFall(String nextShot, int actualShotValue,
                                             int nextShotValue, String firstPinFall) {
        if (firstPinFall.equals(Constants.X) && nextShotValue == Constants.MAX_PINFALL) {
            return Constants.X;
        } else if (actualShotValue + nextShotValue == Constants.MAX_PINFALL && !firstPinFall.equals(Constants.X)) {
            return Constants.SLASH;
        } else {
            return nextShot;
        }
    }

    private String getLastFrameLastPinFall(String afterNextShot, int nextShotValue, int afterNextShotValue,
                                           String firstPinFall) {
        if (firstPinFall.equals(Constants.X) && nextShotValue + afterNextShotValue == Constants.MAX_PINFALL) {
            return Constants.SLASH;
        } else if (afterNextShotValue == Constants.MAX_PINFALL) {
            return Constants.X;
        } else {
            return afterNextShot;
        }
    }
}
