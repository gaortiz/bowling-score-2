package com.jobsity.challenge.service.impl;

import com.jobsity.challenge.model.InputScore;
import com.jobsity.challenge.service.IParserDataService;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

public class ParserDataService implements IParserDataService {

    private static final String WHITE_SPACE = " ";

    public Map<String, List<String>> getGroupedShots(Stream<String> lines) {
        return lines.map(line -> {
            final String name = line.split(WHITE_SPACE)[0];
            final String score = line.split(WHITE_SPACE)[1];
            return new InputScore(name, score);
        }).collect(groupingBy(InputScore::getName, mapping(InputScore::getScore, toList())));
    }
}
