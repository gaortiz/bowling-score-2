package com.jobsity.challenge.service.impl;

import com.jobsity.challenge.exceptions.BowlingScoreException;
import com.jobsity.challenge.exceptions.InvalidFramesException;
import com.jobsity.challenge.exceptions.InvalidScoreException;
import com.jobsity.challenge.model.FramesResult;
import com.jobsity.challenge.service.ILastFrameHandlerService;
import com.jobsity.challenge.service.IScoreCalculator;
import com.jobsity.challenge.utils.Constants;
import com.jobsity.challenge.service.IValidator;

import java.util.*;

public class ScoreCalculator implements IScoreCalculator {

    private static final String F = "F";
    private static final String ZERO_STRING = "0";

    private final ILastFrameHandlerService lastFrameHandlerService = new LastFrameHandlerService();
    private final IValidator validator = new Validator();

    private int convertShotToNumericValue(String shot) throws InvalidScoreException {
        validator.validateScore(shot);
        if (shot.equals(F)) {
            return 0;
        } else {
            return Integer.parseInt(shot);
        }
    }

    public FramesResult calculateScore(final List<String> shots, final String player) throws BowlingScoreException {
        final List<Integer> score = new ArrayList<>();
        final Map<Integer, List<String>> pinFalls = new HashMap<>();
        final int framesLength = shots.size();
        int sum = 0;
        int shotIndex = 0;
        int resultIndex = 1;
        try {
            do {
                String actualShot = shots.get(shotIndex);
                String nextShot = shots.get(shotIndex + 1);
                String afterNextShot = shotIndex + 2 < framesLength ? shots.get(shotIndex + 2) : ZERO_STRING;

                int actualShotValue = convertShotToNumericValue(actualShot);
                int nextShotValue = convertShotToNumericValue(nextShot);
                int afterNextShotValue = convertShotToNumericValue(afterNextShot);

                if (actualShotValue == Constants.MAX_PINFALL) {
                    pinFalls.put(resultIndex, Collections.singletonList(Constants.X));
                }
                if (resultIndex == Constants.TOTAL_FRAMES) {
                    lastFrameHandlerService.setLastFramePinFalls(pinFalls, actualShot, nextShot,
                            afterNextShot, actualShotValue, nextShotValue, afterNextShotValue);
                }

                if (actualShotValue == 10 || resultIndex == Constants.TOTAL_FRAMES) {
                    sum += actualShotValue + nextShotValue + afterNextShotValue;
                    shotIndex++;
                } else if (actualShotValue + nextShotValue == Constants.MAX_PINFALL) {
                    pinFalls.put(resultIndex, Arrays.asList(actualShot, Constants.SLASH));
                    sum += actualShotValue + nextShotValue + afterNextShotValue;
                    shotIndex += 2;
                } else {
                    pinFalls.put(resultIndex, Arrays.asList(actualShot, nextShot));
                    sum += actualShotValue + nextShotValue;
                    shotIndex += 2;
                }

                resultIndex++;
                score.add(sum);

            } while (resultIndex <= Constants.TOTAL_FRAMES);

            return new FramesResult(score, pinFalls, player);
        } catch (IndexOutOfBoundsException e) {
            throw new InvalidFramesException();
        }
    }

}
