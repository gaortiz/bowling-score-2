package com.jobsity.challenge.service.impl;

import com.jobsity.challenge.exceptions.InvalidScoreException;
import com.jobsity.challenge.service.IValidator;

public class Validator implements IValidator {

    private static final String SCORE_REGEXP = "^(?:[0-9]|10|F)$";

    public void validateScore(String score) throws InvalidScoreException {
        if(!score.matches(SCORE_REGEXP)) {
            throw new InvalidScoreException();
        }
    }

}
