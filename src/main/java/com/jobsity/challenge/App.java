package com.jobsity.challenge;

import com.jobsity.challenge.exceptions.BowlingScoreException;
import com.jobsity.challenge.model.FramesResult;
import com.jobsity.challenge.service.IParserDataService;
import com.jobsity.challenge.service.IPrinterService;
import com.jobsity.challenge.service.IScoreCalculator;
import com.jobsity.challenge.service.impl.ParserDataService;
import com.jobsity.challenge.service.impl.PrinterService;
import com.jobsity.challenge.service.impl.ScoreCalculator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class App {

    public static void main(String[] args) {

        final String fileName = args[0];

        final IScoreCalculator scoreCalculator = new ScoreCalculator();
        final IPrinterService printerService = new PrinterService();
        final IParserDataService parserDataService = new ParserDataService();

        try {
            final Stream<String> streamLines = Files.lines(Paths.get(fileName));

            final Map<String, List<String>> groupedShots = parserDataService.getGroupedShots(streamLines);

            final List<FramesResult> framesResults = new ArrayList<>();

            for(Map.Entry<String, List<String>> entry: groupedShots.entrySet()) {
                framesResults.add(scoreCalculator.calculateScore(entry.getValue(), entry.getKey()));
            }

            printerService.printResults(framesResults);
        } catch (IOException e) {
            System.out.print("There was an error reading the file, please fix it or try it with another");
        } catch (BowlingScoreException e) {
            System.out.print(e.getMessage());
        }
    }
}

